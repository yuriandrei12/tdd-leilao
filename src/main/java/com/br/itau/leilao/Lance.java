package com.br.itau.leilao;

public class Lance {
    Usuario usuario = new Usuario();
    Double valorDoLance;

    public Lance(){

    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Double getValorDoLance() {
        return valorDoLance;
    }

    public void setValorDoLance(Double valorDoLance) {
        this.valorDoLance = valorDoLance;
    }
}
