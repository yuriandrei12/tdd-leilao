package com.br.itau.leilao;

import java.util.ArrayList;
import java.util.List;

public class Leilao {
    List<Lance> lances = new ArrayList<>();

    public List<Lance> getLances() {
        return lances;
    }

    public void setLances(List<Lance> lances) {
        this.lances = lances;
    }

    public void adicionarNovoLance(Lance lance){
        lances.add(lance);
    }

    public void validarLance(Lance lance){

    }
}
